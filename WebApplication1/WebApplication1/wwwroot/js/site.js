﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

public function show($alias) {
    $post = Post:: where('alias', $alias) -> first();
    if ($post) {
        $comments = $post -> comments;
        /*
         * Группируем комментарии по полю parent_id. При этом данное поле становится ключами массива 
         * коллекций содержащих модели комментариев
         */
        $com = $comments -> groupBy('parent_id');
    } else $com = false;
    return view('post', [
        'post' => $post,
        'com' => $com
    ]);
}
